package com.template.webserver;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.template.contracts.OrderContract;
import com.template.flows.OrderIssueFlow;
import com.template.flows.RestResponse;
import com.template.flows.SettlementFlow;
import com.template.states.OrderState;
import net.corda.core.contracts.Amount;
import net.corda.core.contracts.BelongsToContract;
import net.corda.core.contracts.LinearState;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.identity.AbstractParty;
import net.corda.core.identity.CordaX500Name;
import net.corda.core.identity.Party;
import net.corda.core.messaging.CordaRPCOps;
import net.corda.core.messaging.FlowHandle;
import net.corda.core.node.services.Vault;
import net.corda.core.node.services.vault.QueryCriteria;
import net.corda.core.transactions.SignedTransaction;
import net.corda.core.utilities.OpaqueBytes;
import net.corda.finance.contracts.asset.Cash;
import net.corda.finance.flows.AbstractCashFlow;
import net.corda.finance.flows.CashIssueFlow;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Currency;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Define your API endpoints here.
 */
@RestController
@RequestMapping("/") // The paths for HTTP requests are relative to this base path.
public class Controller {
    private final CordaRPCOps proxy;
    private final static Logger logger = LoggerFactory.getLogger(Controller.class);

    public Controller(NodeRPCConnection rpc) {
        this.proxy = rpc.proxy;
    }

    @GetMapping(value = "/templateendpoint", produces = "text/plain")
    private String templateendpoint() {
        return "Define an endpoint here.";
    }

    /**
     * Debug endpoint to issue tokens to itself
     *
     * @param amount
     * @param currency
     * @return
     */
    @GetMapping(value = "/selfIssueCash", produces = "application/json")
    public RestResponse<?> selfIssueCash(
            @RequestParam(value = "amount") int amount,
            @RequestParam(value = "currency") String currency) {

        // 1. Prepare issue request.
        final Amount<Currency> issueAmount = new Amount<>((long) amount * 100, Currency.getInstance(currency));
        final List<Party> notaries = proxy.notaryIdentities();
        if (notaries.isEmpty()) {
            throw new IllegalStateException("Could not find a notary.");
        }
        final Party notary = notaries.get(0);
        final OpaqueBytes issueRef = OpaqueBytes.of(new byte[1]);
        final CashIssueFlow.IssueRequest issueRequest = new CashIssueFlow.IssueRequest(issueAmount, issueRef, notary);

        // 2. Start flow and wait for response.
        try {
            final FlowHandle<AbstractCashFlow.Result> flowHandle = proxy.startFlowDynamic(CashIssueFlow.class, issueRequest);
            final AbstractCashFlow.Result result = flowHandle.getReturnValue().get();
            final String msg = result.getStx().getTx().getOutputStates().get(0).toString();
            RestResponse<String> rs = new RestResponse<>();
            rs.setSuccess(true);
            rs.setData(msg);

            return rs;
        } catch (Exception e) {
            RestResponse<String> rs = new RestResponse<>();
            rs.setSuccess(false);
            rs.setError(e);

            return rs;
        }
    }

    @GetMapping(value = "/getBalance", produces = "application/json")
    public RestResponse<?> getBalance(String currency) {
        Vault.Page<Cash.State> page = proxy.vaultQueryByCriteria(new QueryCriteria.FungibleAssetQueryCriteria()
                .withStatus(Vault.StateStatus.UNCONSUMED), Cash.State.class);
        long result = page.getStates().stream()
                .map(s -> s.getState())
                .map(s -> s.getData())
                .map(s -> s.getAmount())
                .filter(a -> currency.equals(a.getToken().getProduct().getCurrencyCode()))
                .map(s -> s.getQuantity())
                .collect(Collectors.summingLong(x -> x));

        Vault.Page<OrderState> page2 = proxy.vaultQueryByCriteria(new QueryCriteria.LinearStateQueryCriteria()
                .withStatus(Vault.StateStatus.UNCONSUMED), OrderState.class);

        long result2 = page2.getStates().stream()
                .map(s -> s.getState())
                .map(s -> s.getData())
                .map(s -> s.getAmount())
                .filter(a -> currency.equals(a.getToken().getCurrencyCode()))
                .map(s -> s.getQuantity())
                .collect(Collectors.summingLong(x -> x));


        RestResponse<Balance> rs = new RestResponse<>();
        Balance balance = new Balance();
        balance.setReserved(result2);
        balance.setTotal(result);
        rs.setData(balance);
        rs.setSuccess(true);
        return rs;
    }

    @GetMapping(value = "/getOrders", produces = "application/json")
    public RestResponse<?> getOrders() {
        Vault.Page<OrderState> page = proxy.vaultQueryByCriteria(new QueryCriteria.LinearStateQueryCriteria()
                .withStatus(Vault.StateStatus.UNCONSUMED), OrderState.class);
        List<OrderStateDto> result = page.getStates().stream()
                .map(s -> s.getState())
                .map(s -> s.getData())
                .map(d -> new OrderStateDto(d))
                .collect(Collectors.toList());


        RestResponse< List<OrderStateDto>> rs = new RestResponse<>();
        rs.setData(result);
        rs.setSuccess(true);
        return rs;
    }

    /**
     * Creates order
     *
     * @return
     */
    @GetMapping(value = "/makeOrder", produces = "application/json")
    public RestResponse<?> makeOrder(
            @RequestParam(value = "amount") int amount,
            @RequestParam(value = "srcCurrency") String srcCurrency,
            @RequestParam(value = "type") int type,
            @RequestParam(value = "tgtCurrency") String tgtCurrency,
            @RequestParam(value = "rate") double rate,
            @RequestParam(value = "orderBook") String orderBook
            ) {

        // 1. Prepare issue request.
        final Amount<Currency> issueAmount = new Amount<>((long) amount * 100, Currency.getInstance(srcCurrency));
        final List<Party> notaries = proxy.notaryIdentities();
        if (notaries.isEmpty()) {
            throw new IllegalStateException("Could not find a notary.");
        }

        Party orderbBookParty = proxy.partiesFromName(orderBook,false).iterator().next();

        final Party notary = notaries.get(0);
        final OpaqueBytes issueRef = OpaqueBytes.of(new byte[1]);
        final OrderIssueFlow.Request issueRequest = new OrderIssueFlow.Request(issueAmount, type, Currency.getInstance(tgtCurrency), rate, orderbBookParty);

        // 2. Start flow and wait for response.
        try {
            final FlowHandle<AbstractCashFlow.Result> flowHandle = proxy.startFlowDynamic(OrderIssueFlow.class, issueRequest);
            final AbstractCashFlow.Result result = flowHandle.getReturnValue().get();
            final String msg = result.getStx().getTx().getOutputStates().get(0).toString();
            RestResponse<String> rs = new RestResponse<>();
            rs.setSuccess(true);
            rs.setData(msg);

            return rs;
        } catch (Exception e) {
            RestResponse<String> rs = new RestResponse<>();
            rs.setSuccess(false);
            rs.setError(e);

            return rs;
        }
    }

    @GetMapping(value = "/settle", produces = "application/json")
    public RestResponse<String> settle(    @RequestParam(value = "buyId") String buyId,
                           @RequestParam(value = "sellId") String sellId
    ) {

                // 2. Start flow and wait for response.

        try {
            final FlowHandle<AbstractCashFlow.Result> flowHandle = proxy.startFlowDynamic(SettlementFlow.SettlementFlowRegistry.class, buyId, sellId);
            final AbstractCashFlow.Result result = flowHandle.getReturnValue().get();
            final String msg = result.getStx().getTx().toString();
            RestResponse<String> rs = new RestResponse<>();
            rs.setSuccess(true);
            rs.setData(msg);

            return rs;
        } catch (Exception e) {
            RestResponse<String> rs = new RestResponse<>();
            rs.setSuccess(false);
            rs.setError(e);

            return rs;
        }

    }

    private static class Balance {
        private long total;
        private long reserved;

        public long getTotal() {
            return total;
        }

        public void setTotal(long total) {
            this.total = total;
        }

        public long getReserved() {
            return reserved;
        }

        public void setReserved(long reserved) {
            this.reserved = reserved;
        }
    }

    private static class OrderStateDto {

        private static final int BUY = 0;
        private static final int CELL = 1;

        /**
         * Order unique identifier
         */
        private final UniqueIdentifier orderId;

        /**
         * Order amount in source currency
         */
        private final Amount<Currency> amount;

        /**
         * Order type (buy/sell)
         */
        private final int type;

        /**
         * Currency to swap to
         */
        private final Currency target;

        /**
         * Exchange rate
         */
        private final double rate;



        public OrderStateDto(OrderState state) {
            this.orderId = state.getOrderId();
            this.amount = state.getAmount();
            this.type = state.getType();
            this.target = state.getTarget();
            this.rate = state.getRate();
        }

        @NotNull
        public UniqueIdentifier getLinearId() {
            return orderId;
        }


        public UniqueIdentifier getOrderId() {
            return orderId;
        }


        public Amount<Currency> getAmount() {
            return amount;
        }

        public int getType() {
            return type;
        }

        public Currency getTarget() {
            return target;
        }

        public double getRate() {
            return rate;
        }

        @Override
        public String toString() {
            return "OrderState{" +
                    "orderId=" + orderId +
                    ", amount=" + amount +
                    ", type=" + type +
                    ", target=" + target +
                    ", rate=" + rate +
                    '}';
        }
    }
}