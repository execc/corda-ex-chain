package com.template.flows;

import co.paralleluniverse.fibers.Suspendable;
import net.corda.core.contracts.Amount;
import net.corda.core.flows.FlowException;
import net.corda.core.flows.FlowLogic;
import net.corda.core.flows.InitiatingFlow;
import net.corda.core.flows.StartableByRPC;
import net.corda.core.identity.Party;
import net.corda.core.utilities.OpaqueBytes;
import net.corda.core.utilities.ProgressTracker;
import net.corda.finance.flows.CashIssueFlow;
import net.corda.finance.flows.CashPaymentFlow;

import java.util.Currency;


@InitiatingFlow
@StartableByRPC
public class IssueFlow extends FlowLogic<Void> {

    private final Amount<Currency> amount;
    private final Party owner;
    private final OpaqueBytes issueRef;

    /**
     * The progress tracker provides checkpoints indicating the progress of the flow to observers.
     */
    private final ProgressTracker progressTracker = new ProgressTracker();

    public IssueFlow(Amount<Currency> amount, OpaqueBytes issueRef, Party owner) {
        this.amount = amount;
        this.owner = owner;
        this.issueRef = issueRef;
    }

    @Override
    public ProgressTracker getProgressTracker() {
        return progressTracker;
    }

    /**
     * The flow logic is encapsulated within the call() method.
     */
    @Suspendable
    @Override
    public Void call() throws FlowException {
        // We retrieve the notary identity from the network map.
        Party notary = getServiceHub().getNetworkMapCache().getNotaryIdentities().get(0);

        subFlow(new CashIssueFlow(amount, issueRef, notary));
        subFlow(new CashPaymentFlow(amount, owner, true, notary));

        return null;
    }
}
