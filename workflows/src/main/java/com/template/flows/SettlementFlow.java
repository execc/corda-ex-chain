package com.template.flows;

import co.paralleluniverse.fibers.Suspendable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.template.contracts.OrderContract;
import com.template.states.OrderState;
import kotlin.Pair;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.crypto.SecureHash;
import net.corda.core.flows.*;
import net.corda.core.identity.Party;
import net.corda.core.identity.PartyAndCertificate;
import net.corda.core.node.NodeInfo;
import net.corda.core.node.StatesToRecord;
import net.corda.core.node.services.Vault;
import net.corda.core.node.services.vault.QueryCriteria;
import net.corda.core.transactions.LedgerTransaction;
import net.corda.core.transactions.SignedTransaction;
import net.corda.core.transactions.TransactionBuilder;
import net.corda.finance.contracts.asset.PartyAndAmount;
import net.corda.finance.flows.AbstractCashFlow;
import net.corda.finance.workflows.asset.CashUtils;

import java.security.SignatureException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static net.corda.core.contracts.ContractsDSL.requireThat;

/**
 */
public class SettlementFlow {

    @InitiatingFlow
    @StartableByRPC
    public static class SettlementFlowRegistry extends FlowLogic<AbstractCashFlow.Result> {

        private String buyId;
        private String sellId;

        public SettlementFlowRegistry(String buyId, String sellId) {
            this.buyId = buyId;
            this.sellId = sellId;
        }

        @Suspendable
        @Override
        public AbstractCashFlow.Result call() throws FlowException {
            // We retrieve the notary identity from the network map.
            Party notary = getServiceHub().getNetworkMapCache().getNotaryIdentities().get(0);


            // Get current OrderState
            //
            QueryCriteria.LinearStateQueryCriteria buyCr = new QueryCriteria.LinearStateQueryCriteria()
                        .withUuid(Lists.newArrayList(UUID.fromString(buyId)))
                        .withStatus(Vault.StateStatus.UNCONSUMED);

            QueryCriteria.LinearStateQueryCriteria sellCr = new QueryCriteria.LinearStateQueryCriteria()
                    .withUuid(Lists.newArrayList(UUID.fromString(sellId)))
                    .withStatus(Vault.StateStatus.UNCONSUMED);

            Vault.Page<OrderState> buyStates = getServiceHub().getVaultService().queryBy(OrderState.class, buyCr);
            Vault.Page<OrderState> sellStates = getServiceHub().getVaultService().queryBy(OrderState.class, sellCr);

            StateAndRef<OrderState> buyState = buyStates.getStates().get(0);
            StateAndRef<OrderState> sellState = sellStates.getStates().get(0);

            // Collect settlement parties
            //
            List<Party> parties = Lists.newArrayList();
            parties.add(buyState.getState().getData().getOrderBookParty());

            // Spent both orders
            //
            TransactionBuilder txn = new TransactionBuilder(notary)
                    .addInputState(buyState)
                    .addInputState(sellState);

            Party target = buyState.getState().getData().getOwner();


            NodeInfo targetNode = getServiceHub().getNetworkMapCache().getNodeByLegalName(target.getName());

            CashUtils.generateSpend(
                    getServiceHub(),
                    txn,
                    Lists.newArrayList(new PartyAndAmount(
                            buyState.getState().getData().getOrderBookParty(),
                            sellState.getState().getData().getAmount())),
                    getOurIdentityAndCert(),
                    Collections.emptySet());

            CashUtils.generateSpend(
                    getServiceHub(),
                    txn,
                    Lists.newArrayList(new PartyAndAmount(
                            sellState.getState().getData().getOwner(),
                            buyState.getState().getData().getAmount())),
                    getOurIdentityAndCert(),
                    Collections.emptySet());

            txn.addCommand(new OrderContract.Commands.Settle(), getOurIdentity().getOwningKey());
            txn.verify(getServiceHub());

            SignedTransaction ptx = getServiceHub().signInitialTransaction(txn);

            FlowSession buyerSession = initiateFlow(buyState.getState().getData().getOrderBookParty());
            //FlowSession sellerSession = initiateFlow(parties.get(0));

            List<FlowSession> counterpartySession = !getServiceHub().getMyInfo().isLegalIdentity(buyState.getState().getData().getOrderBookParty())
                    ? Collections.singletonList(initiateFlow(buyState.getState().getData().getOrderBookParty()))
                    : Collections.singletonList(initiateFlow(buyState.getState().getData().getOwner()));


            //SignedTransaction stx = subFlow(new CollectSignaturesFlow(ptx, counterpartySession));

            subFlow(new FinalityFlow(ptx, counterpartySession));

            return new AbstractCashFlow.Result(ptx, getOurIdentity());
        }
    }


    @InitiatedBy(SettlementFlowRegistry.class)
    public static class SettlementFlowSeller extends FlowLogic<Void> {

        private final FlowSession otherPartySession;

        public SettlementFlowSeller(FlowSession otherPartySession) {
            this.otherPartySession = otherPartySession;
        }

        @Override
        public Void call() throws FlowException {
            class SignTxFlow extends SignTransactionFlow {
                private SignTxFlow(FlowSession otherPartySession) {
                    super(otherPartySession);
                }

                @Override
                protected void checkTransaction(SignedTransaction stx) {
                    requireThat(require -> {
                        return null;
                    });
                }
            }

            SecureHash expectedTxId = subFlow(new SignTxFlow(otherPartySession)).getId();

            subFlow(new ReceiveFinalityFlow(otherPartySession));

            return null;
        }
    }


    /**
     * Buyer review the received settlement transaction then issue the cash to `Seller` party.
     */
    /*
    @InitiatedBy(SettlementFlowSeller.class)
    public static class SettlementFlowBuyer extends FlowLogic<Void> {

        private final FlowSession otherPartySession;

        public SettlementFlowBuyer(FlowSession otherPartySession) {
            this.otherPartySession = otherPartySession;
        }

        @Override
        public Void call() throws FlowException {


            return null;
        }
    }*/
}
