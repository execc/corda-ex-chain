package com.template.flows;

import co.paralleluniverse.fibers.Suspendable;
import net.corda.core.flows.*;

// ******************
// * IOUFlowResponder flow *
// ******************
@InitiatedBy(OrderIssueFlow.class)
public class OrderIssueResponder extends FlowLogic<Void> {
    private final FlowSession otherPartySession;

    public OrderIssueResponder(FlowSession otherPartySession) {
        this.otherPartySession = otherPartySession;
    }

    @Suspendable
    @Override
    public Void call() throws FlowException {
        subFlow(new ReceiveFinalityFlow(otherPartySession));

        return null;
    }
}
