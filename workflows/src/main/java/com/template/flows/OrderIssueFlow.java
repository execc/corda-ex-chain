package com.template.flows;

import co.paralleluniverse.fibers.Suspendable;
import com.template.contracts.OrderContract;
import com.template.states.OrderState;
import net.corda.core.contracts.Amount;
import net.corda.core.contracts.Command;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.flows.*;
import net.corda.core.identity.Party;
import net.corda.core.serialization.CordaSerializable;
import net.corda.core.transactions.SignedTransaction;
import net.corda.core.transactions.TransactionBuilder;
import net.corda.core.utilities.ProgressTracker;
import net.corda.finance.flows.AbstractCashFlow;

import java.util.Collections;
import java.util.Currency;
import java.util.List;

// ******************
// * IOUFlow flow *
// ******************
@InitiatingFlow
@StartableByRPC
public class OrderIssueFlow extends FlowLogic<AbstractCashFlow.Result> {

    private final Amount<Currency> amount;
    private final int type;
    private final Currency target;
    private final double rate;
    private final Party orderBookParty;

    /**
     * The progress tracker provides checkpoints indicating the progress of the flow to observers.
     */
    private final ProgressTracker progressTracker = new ProgressTracker();

    public OrderIssueFlow(Amount<Currency> amount, int type, Currency target, double rate, Party orderBookParty) {
        this.amount = amount;
        this.type = type;
        this.target = target;
        this.rate = rate;
        this.orderBookParty = orderBookParty;
    }

    public OrderIssueFlow(Request request) {
        this(
                request.amount,
                request.type,
                request.target,
                request.rate,
                request.orderBookParty
        );
    }

    @Override
    public ProgressTracker getProgressTracker() {
        return progressTracker;
    }

    /**
     * The flow logic is encapsulated within the call() method.
     */
    @Suspendable
    @Override
    public AbstractCashFlow.Result call() throws FlowException {
        // We retrieve the notary identity from the network map.
        Party notary = getServiceHub().getNetworkMapCache().getNotaryIdentities().get(0);


        long ts = getServiceHub().getClock().instant().getNano();
        OrderState orderState = new OrderState(
                new UniqueIdentifier(String.valueOf(ts)),
                getOurIdentity(),
                amount,
                type,
                target,
                rate,
                orderBookParty
        );

        // We create the transaction components.
        Command command = new Command<>(new OrderContract.Commands.Create(), getOurIdentity().getOwningKey());

        // We create a transaction builder and add the components.
        TransactionBuilder txBuilder = new TransactionBuilder(notary)
                .addOutputState(orderState, OrderContract.ID)
                .addCommand(command);


        // Signing the transaction.
        SignedTransaction signedTx = getServiceHub().signInitialTransaction(txBuilder);

        List<FlowSession> sessions = !getServiceHub().getMyInfo().isLegalIdentity(orderBookParty)
                ? Collections.singletonList(initiateFlow(orderBookParty))
                : Collections.emptyList();

        // We finalise the transaction and then send it to the counterparty.
        subFlow(new FinalityFlow(signedTx, sessions));

        return new AbstractCashFlow.Result(signedTx, getOurIdentity());
    }

    @CordaSerializable
    public static class Request {
        private final Amount<Currency> amount;
        private final int type;
        private final Currency target;
        private final double rate;
        private final Party orderBookParty;

        public Request(Amount<Currency> amount, int type, Currency target, double rate, Party orderBookParty) {
            this.amount = amount;
            this.type = type;
            this.target = target;
            this.rate = rate;
            this.orderBookParty = orderBookParty;
        }

        public Amount<Currency> getAmount() {
            return amount;
        }

        public int getType() {
            return type;
        }

        public Currency getTarget() {
            return target;
        }

        public double getRate() {
            return rate;
        }

        public Party getOrderBookParty() {
            return orderBookParty;
        }
    }
}