package com.template.states;

import com.google.common.collect.ImmutableList;
import com.template.contracts.OrderContract;
import net.corda.core.contracts.Amount;
import net.corda.core.contracts.BelongsToContract;
import net.corda.core.contracts.LinearState;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.identity.AbstractParty;
import net.corda.core.identity.Party;
import org.jetbrains.annotations.NotNull;

import java.util.Currency;
import java.util.List;

@BelongsToContract(OrderContract.class)
public class OrderState implements LinearState {

    private static final int BUY = 0;
    private static final int CELL = 1;

    /**
     * Order unique identifier
     */
    private final UniqueIdentifier orderId;

    /**
     * Who have issued an order
     */
    private final Party owner;

    /**
     * Order amount in source currency
     */
    private final Amount<Currency> amount;

    /**
     * Order type (buy/sell)
     */
    private final int type;

    /**
     * Currency to swap to
     */
    private final Currency target;

    /**
     * Exchange rate
     */
    private final double rate;

    /**
     * Who have issued an order
     */
    private final Party orderBookParty;

    public OrderState(UniqueIdentifier orderId, Party owner, Amount<Currency> amount, int type, Currency target, double rate, Party orderBookParty) {
        this.orderId = orderId;
        this.owner = owner;
        this.amount = amount;
        this.type = type;
        this.target = target;
        this.rate = rate;
        this.orderBookParty = orderBookParty;
    }

    @NotNull
    @Override
    public UniqueIdentifier getLinearId() {
        return orderId;
    }

    @NotNull
    @Override
    public List<AbstractParty> getParticipants() {
        return ImmutableList.of(owner, orderBookParty);
    }

    public UniqueIdentifier getOrderId() {
        return orderId;
    }

    public Party getOwner() {
        return owner;
    }

    public Amount<Currency> getAmount() {
        return amount;
    }

    public int getType() {
        return type;
    }

    public Currency getTarget() {
        return target;
    }

    public double getRate() {
        return rate;
    }

    public Party getOrderBookParty() {
        return orderBookParty;
    }

    @Override
    public String toString() {
        return "OrderState{" +
                "orderId=" + orderId +
                ", owner=" + owner +
                ", amount=" + amount +
                ", type=" + type +
                ", target=" + target +
                ", rate=" + rate +
                ", orderBookParty=" + orderBookParty +
                '}';
    }
}
